/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Annotation.URLannotation;
import Function.ModelView;
import java.util.HashMap;

/**
 *
 * @author Tita
 */
public class Personne {
    private String nom;
    private String prenom;
    private int age;

    public Integer getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Personne(String nom, String prenom, int age) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
    }
    
    

    public Personne() {
    }
    

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public Personne personne()
    {
        return this;
    }
    @URLannotation.url(name="personne")
    public ModelView getPersonne(String nom,String prenom,String age){
        String url = "personne.jsp";
        HashMap<String ,Object> personne = new HashMap<String, Object>();
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setAge(Integer.valueOf(age));
        personne.put("personne",this.personne());
        return new ModelView(url,personne);
    }
    @URLannotation.url(name="test")
    public ModelView getNomFromUrl()
    {
        return null;
    }
}
